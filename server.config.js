module.exports = {
  apps: [
    {
      name: 'Node',
      script: './index.js',
      instances: 0,
      exec_mode: 'fork',
      env: {
        NODE_ENV: 'production',
      }
    }
  ]
};
