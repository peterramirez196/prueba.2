const cookieParser = require('cookie-parser')
const express = require('express')
const session = require('express-session')
const passport = require('passport')
const bodyParser = require('body-parser')
const path = require('path')
const flash = require('connect-flash')
const app = express();
const MemoryStore = session.MemoryStore;
//const config = require("./configuracion")

//config.configuracionInicial()

app.set('views', path.join(__dirname, 'views'))
app.use(express.static(path.join(__dirname, 'resources/')))

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cookieParser())

// Configuración de passport para sesiones

app.use(session({
    secret: 'pesecret',
    resave: true,
    saveUninitialized: true,
    store: new MemoryStore(),
}))

app.use(passport.initialize())
app.use(passport.session())
app.use(flash())


// Luego se podrá acceder a req.user (debe ir luego de initialize y session)
app.use((req, res, next) => {
    res.locals.user = req.user || null
    res.locals.messages = require('express-messages')(req, res)();
    next()
})
app.get('/', (req, res) =>{
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Hello World\n');
})
module.exports = app
